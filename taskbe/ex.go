package main

import (
	"fmt"
	"reflect"
)

func main()  {
	type C struct{
		A string
		B int
	}

	type A struct {
		A string
		B uint
		E C
	}

	type B struct {
		AA string
		B  int
		D A
		E C
	}

	var (
		a = A{
			A: "Тест A",
			B: 55,
			E: C{"Hello", 99},
		}

		b = B{
			AA: "OKOK",
			B:  10,
			D: A{"Inner", 777,  C{"Hello", 2}},
			E: C{"World", 88},
		}
	)

	cnt := uint(0)
	res := Assign1(a, &b, &cnt, 0)
	fmt.Println(res)
}

func Assign1(sour interface{}, dist interface{}, cnt *uint, depth int) uint {
	sVal := reflect.ValueOf(sour)
	dVal := reflect.ValueOf(dist).Elem()
	fmt.Println("iter-", depth)
	fmt.Printf("val1 - %v, type - %s, val2 - %v\n", sVal, sVal.Type(), dVal)
	fmt.Println("typof", reflect.TypeOf(sVal))

	// loop through source
	for i := 0; i < sVal.NumField(); i++ {
		f := sVal.Field(i)

		var fieldName string
		var typ reflect.Type
		fieldName = sVal.Type().Field(i).Name
		typ = f.Type()

		if f.Kind() != reflect.Struct {
			fmt.Println("not struct", sVal)
			var v2FName string
			var v2Typ reflect.Type

			// loop through dist
			for k := 0; k < dVal.NumField(); k++ {
				// todo: check if in visited then skip visited field

				// record name and type to compare
				fv2 := dVal.Field(k)
				v2Typ = fv2.Type()
				v2FName = dVal.Type().Field(k).Name

				if fv2.Kind() != reflect.Struct {
					if fieldName == v2FName && typ == v2Typ {
						// todo: mark as checked for dVal
						fmt.Println("filf", fieldName, i, k, typ, v2Typ)
						dVal.Field(k).Set(sVal.FieldByName(fieldName))
						*cnt++
					}
				} else if fv2.Kind() == reflect.Struct {
					fmt.Printf("dValstruct %v, type -%s, fName - %s\n", fv2, v2Typ, v2FName)
					fmt.Println("dVal", f.Interface())
					//return Assign1(f.Interface(), &fv2, cnt, depth + 1)
				}
			}
		} else if f.Kind() == reflect.Struct {
			fmt.Println("struct1", f, f.Type(), f, fieldName)
			fmt.Println("struct2", f.Interface(),  sVal.Field(i).Type())
			return Assign1(f.Interface(), &dVal, cnt, depth + 1)
		}
	}
	return *cnt
}

