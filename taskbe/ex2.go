package main

import (
	"fmt"
	"reflect"
)

func main()  {
	type C struct{
		A string
		B int
	}

	type A struct {
		A string
		B uint
		E C
	}

	type B struct {
		AA string
		B  int
		D A
		E C
	}

	var (
		a = A{
			A: "Тест A",
			B: 10,
			E: C{"Hello", 99},
		}

		b = B{
			AA: "OKOK",
			B:  55,
			D: A{"Inner", 777,  C{"Hello", 2}},
			E: C{"World", 88},
		}
	)

	res := Check1(b, &a, 0)
	fmt.Println("DONE", res)
}

func Check1(sour interface{}, dist interface{}, iter uint) bool  {
	v1 := reflect.ValueOf(sour)
	//v2 := reflect.ValueOf(dist)
	//fmt.Println(v1.Type())
	//fmt.Println(v2)

	sNum := v1.NumField()
	//sType := v1.Type()
	//fmt.Println("type", sType)

	for i := 0; i < sNum; i++ {
		v := v1.Field(i)
		//fmt.Println("ch", v)
		if v.Kind() == reflect.Struct {
			//fmt.Println("struct", sType, v1, iter)
			//fmt.Println("struct", v, " field ", v1.Field(i))
			Check1(v.Interface(), dist, iter + 1)
		} else {
			fmt.Println("type", v, v.Type())
			//fmt.Println(v)
		}
	}
	return true
}