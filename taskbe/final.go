package main

import (
	"fmt"
	"reflect"
)

func main()  {
	type C struct{
		A string
		B int
	}

	type A struct {
		A string
		B uint
		E C
		C string
	}

	type B struct {
		AA string
		A string
		B  int
		D A
		E C
		C string
	}

	var (
		a = A{
			A: "Тест A",
			B: 10,
			E: C{"Hello333", 99},
			C: "Hi From C",
		}

		b = B{
			AA: "OKOK",
			A: "Тест BB",
			B:  55,
			C: "Im C",
			D: A{"Inner", 777,  C{"TO_TEST_A", 2}, "Hey there"},
			E: C{"TO_TEST_A", 88},
		}
	)

	res := Assign(a, &b)
	fmt.Printf("--- \nres %d\n , v1 - %v\n, v2 - %v\n", res, a, b)
}

func Assign(sour interface{}, dist interface{}) uint {
	v1 := reflect.ValueOf(sour)
	v2 := reflect.ValueOf(dist).Elem()

	cnt := uint(0)
	return deepCheck(v1, &v2, cnt, 0)
}

func deepCheck(sVal reflect.Value, dVal *reflect.Value, cnt uint, depth int) uint {
	// todo check if there is not default values in struct data
	//fmt.Printf("val1 - %v, type - %s, val2 - %v\n", sVal, sVal.Type(), dVal)

	// loop through source
	for i := 0; i < sVal.NumField(); i++ {
		f := sVal.Field(i)

		var fieldName string
		var typ reflect.Type
		fieldName = sVal.Type().Field(i).Name
		typ = f.Type()

		if f.Kind() != reflect.Struct {
			var v2FName string
			var v2Typ reflect.Type

			// loop through dist
			for k := 0; k < dVal.NumField(); k++ {
				// todo: check if in visited then skip visited field
				// record name and type to compare
				fv2 := dVal.Field(k)
				v2Typ = fv2.Type()
				v2FName = dVal.Type().Field(k).Name

				if fv2.Kind() != reflect.Struct {
					if fieldName == v2FName && typ == v2Typ {
						// todo: mark as checked for dVal
						dVal.Field(k).Set(sVal.FieldByName(fieldName))
						cnt++
					}
				} else if fv2.Kind() == reflect.Struct {
					//fmt.Println("vvv", fv2, &fv2)
					dCount := checkSourceFieldAndTypeInDist(fieldName, typ, sVal.FieldByName(fieldName), &fv2)
					cnt += dCount
				}
			}
		} else if f.Kind() == reflect.Struct {
			return deepCheck(f, dVal, cnt, depth + 1)
		}
	}
	return cnt
}

func checkSourceFieldAndTypeInDist(sFieldName string, sType reflect.Type, val reflect.Value, v interface{}) uint {
	cnt := uint(0)
	dVal := reflect.ValueOf(v).Elem()
	fmt.Println(dVal, v)
	num := dVal.NumField()
	for i := 0; i < num; i++ {
		f := dVal.Field(i)

		dType := f.Type()
		dFName := dVal.Type().Field(i).Name

		if f.Kind() != reflect.Struct {
			if sFieldName == dFName && sType == dType {
				dVal.Field(i).Set(val)
				cnt++
			}
		} else if f.Kind() == reflect.Struct {
			fmt.Println("f", f)
			checkSourceFieldAndTypeInDist(sFieldName, sType, val, f.Interface())
		}
	}

	return cnt
}
