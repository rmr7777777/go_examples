package main

import "fmt"


type Wallet struct {
	Amount int
}

func (w *Wallet) Pay(amount int) error  {
	if w.Amount < amount {
		return fmt.Errorf("Not enough money in your waller")
	}

	w.Amount -= amount
	return nil
}

type Payer interface {
	Pay(int) error
}

func Buy(in interface{})  {
	var p Payer
	var ok bool
	if p, ok = in.(Payer); !ok {
		fmt.Printf("%T is not for payment purpose\n\n", in)
		return
	}

	err := p.Pay(10)
	if err != nil {
		fmt.Printf("Error on payment with %T: %v\n\n", p, err)
		return
	}

	fmt.Printf("Thank for buying with %T\n\n", p)
}

func main() {
	wallet := &Wallet{100}
	fmt.Printf("%T\n", wallet)
	Buy(wallet)
}
