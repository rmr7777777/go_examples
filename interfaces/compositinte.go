package main

import "fmt"

type Phone struct {
	Money int
}

func (p *Phone) Pay(amount int) error {
	if p.Money < amount {
		return fmt.Errorf("Not enough money in your phone wallet")
	}

	p.Money -= amount
	return nil
}

func (p *Phone) Ring(ring string) error  {
	if len(ring) == 0 {
		return fmt.Errorf("DO not have ringer")
	}

	return nil
}

type Payer interface {
	Pay(int) error
}

type Ringer interface {
	Ring(string) error
}

type NFCPhone interface {
	Payer
	Ringer
}

func PayWithNFCPhone(phone NFCPhone) {
	err := phone.Pay(10)
	if err != nil {
		fmt.Printf("Error when paying with %T", err)
	}

	fmt.Println("Tanks for paying with NFX PHONE")
}

func main() {
	phone := &Phone{Money: 100}
	PayWithNFCPhone(phone)
}