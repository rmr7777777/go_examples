package main

import (
	"fmt"
	"strconv"
)

type Wallet struct {
	Cash int
}

func (w *Wallet) String() string {
	return "Wallet where " + strconv.Itoa(w.Cash) + " money"
}

func main() {
	myWallet := &Wallet{Cash: 100}
	fmt.Printf("Raw payment: %#v\n", myWallet)
	fmt.Printf("Payment TYpe: %s\n", myWallet)
}
