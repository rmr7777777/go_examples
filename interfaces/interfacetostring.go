package main

import "fmt"

type Credit struct {
	Amount int
}

func (c *Credit) PayLoan(amount int) {
	c.Amount -= amount
}

func (c *Credit) GetLoan (amount int) {
	c.Amount += amount
}

func (c Credit) String() string {
	return fmt.Sprintf("%v", c.Amount)
}

func main() {
	var newLoan = Credit{}
	newLoan.GetLoan(100)
	fmt.Println(newLoan)
}