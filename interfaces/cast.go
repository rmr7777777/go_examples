package main

import "fmt"

type Card struct {
	Balance int
	CardHolder string
}

func (c *Card) Pay(amount int) error {
	if c.Balance < amount {
		return fmt.Errorf("Amount of money in your card less than purchaing price")
	}
	c.Balance -= amount
	return nil
}

type Wallet struct {
	Cash int
}

func (w *Wallet) Pay(amount int) error {
	if w.Cash < amount {
		return fmt.Errorf("Not enough money in your Wallet")
	}

	w.Cash -= amount
	return nil
}

type ApplePay struct {
	Money int
	APpleId string
}

func (a *ApplePay) Pay(amount int) error {
	if a.Money < amount {
		return fmt.Errorf("not enough money in ApllePay")
	}

	a.Money -= amount
	return nil
}

type Payer interface {
	Pay(int) error
}

func Buy(p Payer) {
	switch p.(type) {
	case *Wallet:
		fmt.Printf("In Cash payment")
	case *Card:
		plasticCard, ok := p.(*Card)
		if !ok {
			fmt.Println("Cant cast to *Card type")
		}
		fmt.Println("Insert your card,", plasticCard.CardHolder)
	default:
		fmt.Println("Something new, Suppose to be new payment type")
	}

	err := p.Pay(10)
	if err != nil {
		fmt.Printf("Error on paying %T: %v\n\n", p, err)
	}

	fmt.Printf("Thanks for buying from %T\n\n", p)
}

func main()  {
	myWallet := &Wallet{Cash: 500}
	Buy(myWallet)

	var mPayer Payer

	mPayer = &Card{Balance: 100}
	Buy(mPayer)

	mPayer = &ApplePay{Money: 222}
	Buy(mPayer)
}
