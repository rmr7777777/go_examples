package main

import "fmt"

type Wallet struct {
	Cash int
}

func (w *Wallet) Pay(amount int) error  {
	if w.Cash < amount {
		return fmt.Errorf("Not enough money in wallet")
	}

	w.Cash -= amount
	return nil
}

type Card struct {
	Balance int
	ValidUntil string
	Cardholder string
	CVV string
	Number string
}

func (c *Card) Pay(amount int) error {
	if c.Balance < amount {
		return fmt.Errorf("Not enough money in card")
	}
	c.Balance -= amount
	return nil
}

type ApplePay struct {
	Money int
	AppleId string
}

func (a *ApplePay) Pay(amount int) error {
	if a.Money < amount {
		return fmt.Errorf("Not enough money in Apple Pay")
	}

	a.Money -= amount
	return nil
}


type Payer interface {
	Pay(int) error
}

func Buy(p Payer, amount int)  {
	err := p.Pay(amount)
	if err != nil {
		fmt.Printf("Error on paying %T: %v\n\n", p, err)
	}

	fmt.Printf("Thanks for buying from %T\n\n", p)
}

func main()  {
	myWallet := &Wallet{Cash: 500}
	Buy(myWallet, 200)

	var mPayer Payer

	mPayer = &Card{Balance: 100}
	Buy(mPayer, 10)

	mPayer = &ApplePay{Money: 222}
	Buy(mPayer, 50)

	//myCard := &Card{Balance: 100}
	//Buy(myCard, 10)
	//
	//myApplePay := &ApplePay{Money: 100}
	//Buy(myApplePay, 96)
}
