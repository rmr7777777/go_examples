package main

import "fmt"

type Account struct {
	Id string
	Name string
	Cleaner func(string) string
	Owner Person
	Person
}

type Person struct {
	 Name string
	 Age int
	 City string
}


func main () {
	var acc Account = Account {
		Id: "12312-2312d",
		Person: Person {
			Name: "R. Benitez",
		},
	}

	acc.Owner = Person{"RafaBenitez", 29, "Almaty"}
	//acc.Cleaner = func(s string) string {
	//	fmt.Printf(s)
	//	return s
	//}
	fmt.Printf("%v\n", acc)
	fmt.Printf(acc.Name)
}
