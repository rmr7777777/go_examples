package main

import (
	"bufio"
	"bytes"
	"strings"
	"testing"
)

var testOk = `1
2
3
4
5
6`

var testOkResult = `1
2
3
4
5
6
`

func TestOk(t *testing.T) {
	in := bufio.NewReader(strings.NewReader(testOk))
	out := new(bytes.Buffer)

	err := uniq(in, out)

	if err != nil {
		t.Errorf("test failed")
	}

	if out.String() != testOkResult {
		t.Errorf("test failed - results not match %v %v", out.String(), testOkResult)
	}
}

var testFail = `1
2
1`

func TestForError(t *testing.T) {
	in := bufio.NewReader(strings.NewReader(testFail))
	out := new(bytes.Buffer)

	err := uniq(in, out)

	if err == nil {
		t.Errorf("test failed: %v", err)
	}
}

type C struct{
	A string
	B int
}

type A struct {
	A string
	B uint
	E C
}

type B struct {
	AA string
	A string
	B  int
	D A
	E C
}

var (
	a = A{
		A: "Тест A",
		B: 10,
		E: C{"Hello", 99},
	}

	b = B{
		AA: "OKOK",
		A: "Тест BB",
		B:  55,
		D: A{"Inner", 777,  C{"Hello", 2}},
		E: C{"World", 88},
	}
)