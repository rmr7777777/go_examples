package main

import (
	"bufio"
	"fmt"
	"io"
	"os"
)

func main() {
	err := uniq(os.Stdin, os.Stdout)
	if err != nil {
		panic("err happened")
	}
}

func uniq(input io.Reader, output io.Writer) error {
	in := bufio.NewScanner(input)
	var prevStr string

	for  in.Scan() {
		txt := in.Text()

		if txt == prevStr {
			continue
		}

		if txt < prevStr {
			return fmt.Errorf("file not sorted")
		}

		prevStr = txt
		fmt.Fprintln(output, txt)
	}
	return nil
}