package main

import (
	"fmt"
	"time"
)

func main() {
	ticker := time.NewTicker(time.Second)
	i := 0

	for tickTime := range ticker.C {
		i++
		fmt.Println("step", i, "time", tickTime)

		if i >= 5 {
			ticker.Stop()
			break
		}
	}

	fmt.Println("total", i)

	// infinite ticking, cant's stop this
	// use if task needed work forever
	// ex. assume you need to check every 1 minute some metric
	// and send it to some service, then can use it
	c := time.Tick(time.Second)

	for tickTime := range c {
		i++
		fmt.Println("step", i, "time", tickTime)

		if (i >= 5) {
			break
		}
	}
}
