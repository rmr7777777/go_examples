package main

import "fmt"

func main() {
	// not bufferized channel
	//ch1 := make(chan int) //ch1 := make(chan int, 0)


	// буферизированный канал
	ch1 := make(chan int, 1)


	go func(in chan int) {
		val := <-in
		fmt.Println("GO: get from chan", val)
		fmt.Println("GO: after read from chan")
	}(ch1)

	//go func(in chan int) {
	//	val := <-in
	//	fmt.Println("GO: get from chan", val)
	//	fmt.Println("GO: after read from chan")
	//}(ch1)

	ch1 <- 42
	ch1 <- 10000

	fmt.Println("MAIN: after put to chan")
	fmt.Scanln()
}
