package mux

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"github.com/gorilla/mux"
)

func main()  {
	handleRequest()
}

/*
	routing with mux
*/
func handleRequest() {
	r := mux.NewRouter().StrictSlash(true)
	r.HandleFunc("/", homePage)
	r.HandleFunc("/all", returnAllArticles)
	r.HandleFunc("/article/{key}/{key1}/{key2}", returnSingleArticle)
	http.Handle("/", r)
	log.Fatal(http.ListenAndServe(":8081", nil))
}

func homePage(w http.ResponseWriter, r *http.Request) {
	fmt.Fprint(w, "Welcome to home page")
	fmt.Println("Endpoint hit: Homepage")
}

func returnSingleArticle(w http.ResponseWriter, r *http.Request)  {
	vars := mux.Vars(r)
	key := vars["key"]
	key1 := vars["key1"]
	key2 := vars["key2"]
	fmt.Fprint(w, "Key: " + key)
	fmt.Fprint(w, "Key: " + key1)
	fmt.Fprint(w, "Key: " + key2)
}

type Article struct {
	Title string `json:"Title"`
	Desc string `json:"desc"`
	Content string `json:"content"`
}

type Articles []Article

func returnAllArticles(w http.ResponseWriter, r *http.Request){
	articles := Articles{
		Article{Title: "Hello", Desc: "Article Description", Content: "Article Content"},
		Article{Title: "Hello 2", Desc: "Article Description", Content: "Article Content"},
	}
	fmt.Println("Endpoint Hit: returnAllArticles")

	json.NewEncoder(w).Encode(articles)
}

/*
without mux
func handleRequest() {
	http.HandleFunc("/", homePage)
	http.HandleFunc("/all", returnAllArticles)
	log.Fatal(http.ListenAndServe(":8081", nil))
}


*/



