package testtaskbe

import "testing"

// TestAssign тестирование функции Assign
func TestAssign(t *testing.T) {
	type A struct {
		A string
		B uint
		C string
	}
	type B struct {
		AA string
		B  int
		C  string
	}
	var (
		a = A{
			A: "Тест A",
			B: 55,
			C: "Test C",
		}
		b = B{
			AA: "OKOK",
			B:  10,
			C:  "FAFA",
		}
	)
	result := Assign(a, &b)
	switch true {
	case b.B != 10:
		t.Errorf("b.B = %d; необходимо 10", b.B)
	case b.C != "Test C":
		t.Errorf("b.C = %v; необходимо 'Test C'", b.C)
	case result != 1:
		t.Errorf("Assign(a,b) = %d; необходимо 1", result)
	}
}

func AssignWithCompositeStr(t *testing.T) {
	type C struct{
		A string
		B int
	}

	type A struct {
		A string
		B uint
		E C
		C string
	}

	type B struct {
		AA string
		A string
		B  int
		D A
		E C
		C string
	}

	var (
		a = A{
			A: "Тест A",
			B: 10,
			E: C{"Hello", 99},
			C: "Hi From C",
		}

		b = B{
			AA: "OKOK",
			A: "Тест BB",
			B:  55,
			C: "Im C",
			D: A{"Inner", 777,  C{"Hello", 2}, "Hey there"},
			E: C{"World", 88},
		}
	)

	result := Assign(a, &b)

	switch true {
	case b.B != 99:
		t.Errorf("b.B = %d; необходимо 99", b.B)
	case b.C != "Hi From C":
		t.Errorf("b.C = %v; необходимо 'Hi From C'", b.C)
	case result != 3:
		t.Errorf("Assign(a,b) = %d; необходимо 1", result)
	}
}
