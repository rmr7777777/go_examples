package testtaskbe

import (
	"fmt"
	"reflect"
)

/* Assign Функция присавивания одинаковых полей
Задача реализовать функцию которая сравнивает две структуры,
находит одинаковые по названию и типу поля и из источника sour
присваевает эти поля в получателя dist.
Также возвращает количество совпадающих нахваний полей с типами.

Если будет возможность то постараться пройтись по всем уровням вложенности
*/
func AssignNotComposite(sour interface{}, dist interface{}) uint {
	cnt  := uint(0)

	sVal := reflect.ValueOf(sour)
	dVal := reflect.ValueOf(dist).Elem()

	for i := 0; i < sVal.NumField(); i++ {
		f := sVal.Field(i)

		var fieldName string
		var typ reflect.Type

		if f.Kind() != reflect.Struct {
			fieldName = sVal.Type().Field(i).Name
			typ = f.Type()

			var v2FName string
			var v2Typ reflect.Type

			fmt.Println("v2", dVal.CanSet(), dVal.NumField())

			for k := 0; k < dVal.NumField(); k++ {
				fv2 := dVal.Field(k)
				v2FName = dVal.Type().Field(k).Name
				v2Typ = fv2.Type()

				if fieldName == v2FName && typ == v2Typ {
					//fmt.Println(fieldName, i, k, typ, v2Typ)
					dVal.Field(k).Set(sVal.FieldByName(fieldName))
					cnt++
				}
			}
		} else if f.Kind() == reflect.Struct {
			fmt.Println("struct")
			vd := f
			for j := 0; j < vd.NumField(); j++ {
				f1 := vd.Type()
				f1v := vd.Field(j)
				fmt.Println(f1.Field(j).Name, f1v, j)
				//fmt.Println(vd.Type())
			}
		}
	}
	return cnt
}

func Assign(sour interface{}, dist interface{}) uint {
	v1 := reflect.ValueOf(sour)
	v2 := reflect.ValueOf(dist).Elem()

	cnt := uint(0)
	return deepCheck(v1, v2, cnt, 0)
}

func deepCheck(sVal, dVal reflect.Value, cnt uint, depth int) uint {
	// todo check if there is not default values in struct data
	fmt.Printf("val1 - %v, type - %s, val2 - %v\n", sVal, sVal.Type(), dVal)

	// loop through source
	for i := 0; i < sVal.NumField(); i++ {
		f := sVal.Field(i)

		var fieldName string
		var typ reflect.Type
		fieldName = sVal.Type().Field(i).Name
		typ = f.Type()

		if f.Kind() != reflect.Struct {
			var v2FName string
			var v2Typ reflect.Type

			// loop through dist
			for k := 0; k < dVal.NumField(); k++ {
				// todo: check if in visited then skip visited field
				// record name and type to compare
				fv2 := dVal.Field(k)
				v2Typ = fv2.Type()
				v2FName = dVal.Type().Field(k).Name

				if fv2.Kind() != reflect.Struct {
					if fieldName == v2FName && typ == v2Typ {
						// todo: mark as checked for dVal
						dVal.Field(k).Set(sVal.FieldByName(fieldName))
						cnt++
					}
				} else if fv2.Kind() == reflect.Struct {
					dCount := checkSourceFieldAndTypeInDist(fieldName, typ, sVal.FieldByName(fieldName),  &fv2)
					cnt += dCount
				}
			}
		} else if f.Kind() == reflect.Struct {
			return deepCheck(f, dVal, cnt, depth + 1)
		}
	}
	return cnt
}

func checkSourceFieldAndTypeInDist(sFieldName string, sType reflect.Type, val reflect.Value, v interface{}) uint {
	cnt := uint(0)
	dVal := reflect.ValueOf(v).Elem()
	fmt.Println(dVal, v)
	num := dVal.NumField()
	for i := 0; i < num; i++ {
		f := dVal.Field(i)

		dType := f.Type()
		dFName := dVal.Type().Field(i).Name

		if f.Kind() != reflect.Struct {
			if sFieldName == dFName && sType == dType {
				dVal.Field(i).Set(val)
				cnt++
			}
		} else if f.Kind() == reflect.Struct {
			fmt.Println("f", f)
			checkSourceFieldAndTypeInDist(sFieldName, sType, val, f.Interface())
		}
	}

	return cnt
}