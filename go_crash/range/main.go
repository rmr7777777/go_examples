package main

import "fmt"

func main() {
	ids := []int{1, 2, 3, 4, 5, 67}

	for i, id := range ids {
		fmt.Printf("%d - ID: %d\n", i, id)
	}

	for _, id := range ids {
		fmt.Printf("ID: -%d\n", id)
	}

	sum := 0
	for _, id := range ids {
		sum += id
	}

	fmt.Println("sum", sum)

	emails := map[string]string{"Bob": "bo@gmail.com", "John": "jS@gmail.com", "Tom": "tc@gmail.com"}

	for k, v := range emails {
		fmt.Printf("%s: %s\n", k, v)
	}

	for _, v := range emails {
		fmt.Println("Name - " + v)
	}

	for k := range emails {
		fmt.Println("Name - " + k)
	}
}
