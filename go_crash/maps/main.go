package main

import "fmt"

func main() {
	emails := make(map[string]string)

	emails["Bob"] = "bb@gmail.com"
	emails["John"] = "js@gmail.com"

	fmt.Println(emails)
	fmt.Println(len(emails))
	fmt.Println(emails["Bob"])

	// delete
	delete(emails, "Bob")
	fmt.Println(emails)


	fans := map[string]string{"Bob": "John Snow", "John": "Sam Smith"}

	fans["Rafa"] = "Tom Cruse"
	fmt.Println(fans)
	fmt.Println(len(fans))
	fmt.Println(fans["Bob"])
}

