package main

import (
	"fmt"
	"strconv"
)

type Person struct {
	//fName string
	//lName string
	//city string
	//gender string
	//age int

	fName, lName, city, gender string
	age int
}

func (p Person) greet() string {
	return "Hello, my name is " + p.lName + " " + p.fName + " I am " + strconv.Itoa(p.age)
}

func (p *Person) getMerried(spauseLName string) {
	if p.gender == "m" {
		return
	} else {
		p.lName = spauseLName
	}
}

func (p *Person) hasBirthday()  {
	p.age++
}

func main() {
	person1 := Person{fName: "Raf", lName: "Ben", city: "Alm", gender: "m", age: 200}
	person2 := Person{fName: "Ri", lName: "Abd", city: "Alm", gender: "f", age: 200}
	//person3 := Person{"Ma", "Ben", "Alm", "Male",  200}

	fmt.Println(person1)
	fmt.Println(person2.lName)
	//person1.hasBirthday()
	//person1.hasBirthday()
	//person1.hasBirthday()
	person2.getMerried(person1.lName)
	fmt.Println(person2.greet())
}
