package main

import "fmt"

func main()  {
	// arrays
	//var fruitArr [2]string
	//
	//fruitArr[0] = "Apple"
	//fruitArr[1] = "Orange"

	fruitArr := [2]string{"Apple", "Orange"}

	fmt.Println(fruitArr)
	fmt.Println(fruitArr[1])
	fmt.Println(fruitArr)


	fruiteSlice := []string{"Apple", "Orange", "Grape"}
	fmt.Println(fruiteSlice, len(fruiteSlice))
	fmt.Println(fruiteSlice[1:2])
}
