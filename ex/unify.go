package main

import (
	b "bufio"
	"fmt"
	"os"
)

func main() {
	in := b.NewScanner(os.Stdin)
	alreadySeen := make(map[string]bool)

	for in.Scan() {
		txt := in.Text()

		if _, found := alreadySeen[txt]; found {
			continue
		}

		alreadySeen[txt] = true
	}
	fmt.Println(alreadySeen)
}