package main

import (
	"fmt"
)

const (
	_  = iota
	KB uint64 =  1 << (10 * iota)
	MB
)

type UserId = int

func main() {
	a := 1
	b := &a
	*b = 3
	c := &a


	d := new(int)
	*d = 12

	*c = *d

	fmt.Println(a, *c, *b)
	*d = 3

	c = d

	fmt.Println(a, *c, *b)
	fmt.Println(&a, c, b)

	*c = 14



	return
	helloWorld := "Привет, HelloWorld"
	//var cnt = len(hello);
	//var bCnt = utf8.RuneCountInString(hello)
	//fmt.Printf("%d - len", cnt)
	//fmt.Printf("%d - len", bCnt)
	//fmt.Println(hello[:11])

	// конвертация в слайс байт и обратно
	byteString := []byte(helloWorld)
	helloWorld = string(byteString)

	fmt.Println(byteString, helloWorld)
	H := helloWorld[0]
	fmt.Println(H)

	hello := helloWorld[:12]

	fmt.Println(hello)
	H = helloWorld[0]

	fmt.Println(H)

	return
	idx := 1
	var uid UserId = 42

	myID := UserId(idx)

	fmt.Println(uid, myID)

	return
	fmt.Println(KB, MB)

	return

}
