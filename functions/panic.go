package main

import "fmt"

func deferTest()  {
	// handle panic
	defer func() {
		if err := recover(); err != nil {
			fmt.Println("panic happend", err)
		}
	}()

	fmt.Println("sume useful work staring")
	panic("smth bad happend")
	return
}

func main () {
	//defer fmt.Println("After work2")
	//defer fmt.Println("After work1")
	//defer func (in string) {
	//	println(in)
	//}("string helow anon")
	deferTest()
	return

	//fmt.Println("Some useful work")
}
