package main

import "fmt"

func doNothing() {
	fmt.Println("hello world")
}

func main () {
	doNothing()

	// anonym foo
	func(in string) {
		fmt.Println(in)
	}("hello my friend ananym")

	//printer := func(in string) {
	//	fmt.Println("printer outs", in)
	//}

	//printer("as a variable")

	printWithError := func(a string) {
		fmt.Println("printer with error", a)
	}


	// foo type
	type strFuncType = func(string)

	worker := func(callback strFuncType) {
		callback("as callback")
	}

	worker(printWithError)

	// closures

	prefixer := func(prefix string) strFuncType {
		return func(in string) () {
			fmt.Printf("[%s] %s\n", prefix, in)
		}
	}

	successLogger := prefixer("SUCCESS")
	successLogger("expected behavior")
	successLogger("expected behavior2")
	successLogger("expected behavior2")

}
